var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded(({extended: false})))

var Message = mongoose.model('Message', {
    name: String,
    message: String
});
var dbUrl = 'mongodb://user:password1@ds161062.mlab.com:61062/learning-node'


app.get('/messages', (req, res) =>{
    Message.find({}, (err, messages) =>{
        res.send(messages);
    });
});

app.post('/messages', async (req, res) =>{
    
    try{
        var message = new Message(req.body);

        var savedMessage = await message.save()
       
        var censord = await Message.findOne({message: 'badword'});
        if (censord)   
            await Message.remove({ _id: censord.id });
        else
            io.emit('messsage', req.body);
    
        res.sendStatus(200);
    
    
    }catch (err){
        res.sendStatus(500)
        return console.error(err)
    } finally {
    }
});

app.get('/messages/:user', (req, res) =>{
    var user = req.params.user;
    Message.find({ name: user }, (err, messages) =>{
        res.send(messages);
    });
});


io.on('connection', (socket) =>{

});

mongoose.connect(dbUrl, { useNewUrlParser: true } , (err) =>{
    console.log('mongog db connection', err);
});

var server = http.listen(3000, () =>{
    console.log('server is listening on port', server.address().port);
});